function [conf] = evaluateRecognitionSystem()
% Evaluates the recognition system for all test-images and returns the confusion matrix

	load('vision.mat');
	load('../data/traintest.mat');

	% TODO Implement your code here
    noOfScenes = size(mapping,2);
    imageFileNameSet = strcat('../data/',test_imagenames);
    confusionMatrix = zeros(noOfScenes,noOfScenes);
    correctIdentification = 0;
    for imgFilenameIdx = 1:size(imageFileNameSet)
        scene = guessImage(imageFileNameSet{imgFilenameIdx});
        sceneId = find(strcmp(mapping,scene));
        actualSceneId = test_labels(imgFilenameIdx);
        actualScene = mapping{actualSceneId};
        if strcmp(scene,actualScene)
            correctIdentification = correctIdentification+1;
        end
        fprintf('Identified : %i / %i \n',correctIdentification,imgFilenameIdx);
        confusionMatrix(sceneId,actualSceneId) = confusionMatrix(sceneId,actualSceneId) + 1;
    end
    accuracy = correctIdentification/size(test_labels,1);
    fprintf('System Accuracy : %f \n',accuracy);
    disp(confusionMatrix);
end