function [wordMap] = getVisualWords(img, filterBank, dictionary)
% Compute visual words mapping for the given image using the dictionary of visual words.

% Inputs:
% 	img: Input RGB image of dimension (h, w, 3)
% 	filterBank: a cell array of N filters
% Output:
%   wordMap: WordMap matrix of same size as the input image (h, w)

    %start of testing code
    img =imread('../data/ice_skating/sun_advbapyfkehgemjf.jpg');
    load('dictionary.mat','filterBank','dictionary');
    [filterBank] = createFilterBank();
    % end of testing code

    % TODO Implement your code here
    filterResponses = extractFilterResponses(img,filterBank);
    sizeOfFilterResponses = size(filterResponses);
    filterResponses = reshape(filterResponses, [sizeOfFilterResponses(1) * sizeOfFilterResponses(2) , sizeOfFilterResponses(3)]);
    distances = pdist2(filterResponses,dictionary');
    [~, wordMap] = min(distances, [], 2);
    
    wordMap = reshape(wordMap, [size(img,1), size(img,2)]);
    
    %spm
    L = 2;
    layerNum = L+1;
    dictionarySize = size(dictionary,2);
    
    histArray = zeros(((4^layerNum)-1)/3,dictionarySize);
    histArr= [];
    [h,histArr] = dynamicRecursiveCall(L,0,wordMap,histArr,dictionarySize);
    
end

function [weight] = getWeightForLayer(l,L)
    if l == 0
        l = 1;
    end
    weight = 2^(l - (L-1) -1);
end

function [histMap] = addHistToArr(h,L,currentLayer,currHistArr)
    histMap = currHistArr;
    histMap(size(histMap,1)+1,:) = getWeightForLayer(currentLayer,L) * h;
end

function [h,histArr] = dynamicRecursiveCall(L , currentLayer, wordMap, currHistArr, dictionarySize)
    if(currentLayer >= L)
        h = getImageFeatures(wordMap,dictionarySize);
        histArr = addHistToArr(h,L,currentLayer,currHistArr);
        return;
    end
    imSplit = [];
    img1 = wordMap(1:end/2, 1:end/2);
    img2 = wordMap(1:end/2, end/2+1:end);
    img3 = wordMap(end/2+1:end, 1:end/2);
    img4 = wordMap(end/2+1:end, end/2+1:end);
    imSplit(:,:,1) = img1;
    imSplit(:,:,2) = img2;
    imSplit(:,:,3) = img3;
    imSplit(:,:,4) = img4;
    h = zeros(1,dictionarySize);
    for splitIdx = 1:4
        [splitHisto,currHistArr] = dynamicRecursiveCall(L,currentLayer+1,imSplit(:,:,splitIdx),currHistArr,dictionarySize);
        h = h + splitHisto;
    end
    h=h/4; %%check
    histArr = addHistToArr(h,L,currentLayer,currHistArr);
    
end
