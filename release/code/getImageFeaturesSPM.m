function [h] = getImageFeaturesSPM(layerNum, wordMap, dictionarySize)
% Compute histogram of visual words using SPM method
% Inputs:
%   layerNum: Number of layers (L+1)
%   wordMap: WordMap matrix of size (h, w)
%   dictionarySize: the number of visual words, dictionary size
% Output:
%   h: histogram of visual words of size {dictionarySize * (4^layerNum - 1)/3} (l1-normalized, ie. sum(h(:)) == 1)

    % TODO Implement your code here
    L = layerNum -1;
    
    histArr= [];
    [~,h] = dynamicRecursiveCall(L,0,wordMap,histArr,dictionarySize);
    sizeOfHistograms = size(h);
    h = reshape(h,[sizeOfHistograms(1)*sizeOfHistograms(2),1]);
    h=h/sum(h(:));

end

function [weight] = getWeightForLayer(l,L)
    if l == 0
        l = 1;
    end
    weight = 2^(l - (L-1) -1);
end

function [histArr] = addHistToArr(h,L,currentLayer,currHistArr)
    histArr = currHistArr;
    histArr(:,size(histArr,2)+1) = getWeightForLayer(currentLayer,L) * h';
end

function [h,histArr] = dynamicRecursiveCall(L , currentLayer, wordMap, currHistArr, dictionarySize)
    if(currentLayer >= L)
        h = getImageFeatures(wordMap,dictionarySize);
        histArr = addHistToArr(h,L,currentLayer,currHistArr);
        return;
    end
    imSplit = [];
    img1 = wordMap(1:end/2, 1:end/2);
    img2 = wordMap(1:end/2, end/2+1:end);
    img3 = wordMap(end/2+1:end, 1:end/2);
    img4 = wordMap(end/2+1:end, end/2+1:end);
    imSplit(:,:,1) = img1;
    imSplit(:,:,2) = img2;
    imSplit(:,:,3) = img3;
    imSplit(:,:,4) = img4;
    h = zeros(1,dictionarySize);
    for splitIdx = 1:4
        [splitHisto,currHistArr] = dynamicRecursiveCall(L,currentLayer+1,imSplit(:,:,splitIdx),currHistArr,dictionarySize);
        h = h + splitHisto;
    end
    h=h/4;
    histArr = addHistToArr(h,L,currentLayer,currHistArr);
    
end
