function [filterResponses] = extractFilterResponses(img, filterBank)
% Extract filter responses for the given image.
% Inputs: 
%   img:                a 3-channel RGB image with width W and height H
%   filterBank:         a cell array of N filters
% Outputs:
%   filterResponses:    a W x H x N*3 matrix of filter responses


% TODO Implement your code here
%start of testing code
%img =imread('../data/ice_skating/sun_advbapyfkehgemjf.jpg');
%[filterBank] = createFilterBank();
% end of testing code

    if length(size(img)) < 3
        newImg=[];
        newImg(:,:,1) = img;
        newImg(:,:,2) = img;
        newImg(:,:,3) = img;
        img = newImg;
    end
    imlab = RGB2Lab(img);

    filterResponses = [];
    for filterIdx = 1 : size(filterBank)
        filter = cell2mat(filterBank(filterIdx));
        imSize = size(imlab);
        for channelIdx = 1:imSize(3)
            imComponent = imlab(:,:,channelIdx);
            filterImComponent = imfilter(imComponent,filter,'replicate');
            filterResponses = cat(3,filterResponses,filterImComponent);
        end
    end
end


