function [filterBank, dictionary] = getFilterBankAndDictionary(imPaths)
% Creates the filterBank and dictionary of visual words by clustering using kmeans.

% Inputs:
%   imPaths: Cell array of strings containing the full path to an image (or relative path wrt the working directory.
% Outputs:
%   filterBank: N filters created using createFilterBank()
%   dictionary: a dictionary of visual words from the filter responses using k-means.

    filterBank  = createFilterBank();

    % TODO Implement your code here
    %Start of testing code
    %imPath = '../data/ice_skating/sun_advbapyfkehgemjf.jpg';
    %End of testing code
    
    alpha = 100;
    k=150;
    filterBankSize = size(filterBank);
    noOfFilterResponses = filterBankSize(1) * 3;
    noOfTrainingImages = length(imPaths);
    
    sampledFilterResponses=zeros(alpha,noOfTrainingImages,noOfFilterResponses);
    for imPathIdx = 1:noOfTrainingImages
        imPath = imPaths(imPathIdx);
        img =imread(imPath{:});
        sizeOfImg = size(img);
        noOfPixels = sizeOfImg(1) * sizeOfImg(2);
        randomPixels = randperm(noOfPixels,alpha);
        filterResponses = extractFilterResponses(img,filterBank);
        filterResponsesSize = size(filterResponses);
        noOfFilterResponses = filterResponsesSize(3);
        for filterResponseIdx = 1: noOfFilterResponses
            pixelValsAtIndices=zeros(1,alpha);
            responseImg = filterResponses(:,:,filterResponseIdx);
            for pixelIdx = 1 : alpha
                pixelValsAtIndices(pixelIdx) = responseImg(randomPixels(pixelIdx));
            end
            sampledFilterResponses(:,imPathIdx,filterResponseIdx) = pixelValsAtIndices;
            
        end
    end
    sampledFilterResponses = reshape(sampledFilterResponses,[alpha*noOfTrainingImages,noOfFilterResponses]);
    [~, dictionary] = kmeans(sampledFilterResponses,k);
    dictionary = dictionary';

end
