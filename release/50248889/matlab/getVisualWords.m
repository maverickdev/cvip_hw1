function [wordMap] = getVisualWords(img, filterBank, dictionary)
% Compute visual words mapping for the given image using the dictionary of visual words.

% Inputs:
% 	img: Input RGB image of dimension (h, w, 3)
% 	filterBank: a cell array of N filters
% Output:
%   wordMap: WordMap matrix of same size as the input image (h, w)

    %start of testing code
    %img =imread('../data/ice_skating/sun_advbapyfkehgemjf.jpg');
    %load('dictionary.mat','filterBank','dictionary');
    %[filterBank] = createFilterBank();
    % end of testing code

    % TODO Implement your code here
    filterResponses = extractFilterResponses(img,filterBank);
    sizeOfFilterResponses = size(filterResponses);  %h x w x 3F
    filterResponses = reshape(filterResponses, [sizeOfFilterResponses(1) * sizeOfFilterResponses(2) , sizeOfFilterResponses(3)]);
    distances = pdist2(filterResponses,dictionary');
    [~, wordMap] = min(distances, [], 2);
    wordMap = reshape(wordMap, [sizeOfFilterResponses(1), sizeOfFilterResponses(2)]);
end