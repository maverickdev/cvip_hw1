function buildRecognitionSystem()
% Creates vision.mat. Generates training features for all of the training images.

	load('dictionary.mat');
	load('../data/traintest.mat');

	% TODO create train_features
    filterBank = createFilterBank();
    train_features = [];
    for imgFilenameIdx = 1:size(train_imagenames,1)
        imgFilename = train_imagenames(imgFilenameIdx);
        imgFilename = strrep(imgFilename,'.jpg','.mat');
        imgFilename = strcat('../data/',imgFilename);
        wordMap = load(imgFilename{:});
        wordMap = wordMap.wordMap;
        h = getImageFeaturesSPM(3, wordMap, size(dictionary,2));
        h=reshape(h,[size(h,1)*size(h,2),1]);
        train_features(:,size(train_features,2)+1) = h;
    end

	save('vision.mat', 'filterBank', 'dictionary', 'train_features', 'train_labels');

end